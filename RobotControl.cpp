/**
* @file RobotControl.cpp
* @author �ulenur ��rez (152120181038)
* @date 23.01.2021
* @brief Source file of RobotControl class.
*
*	This file includes all the implementations declared in the RobotControl header file.
*/



#include "RobotControl.h"

void RobotControl::turnLeft()
{
	if (access)
		robotInterface->turnLeft();
	else
		cout << "You don't have access. Open system preferences for access" << endl;
}

void RobotControl::turnRight()
{
	if (access)
		robotInterface->turnRight();
	else
		cout << "You don't have access. Open system preferences for access" << endl;
}

void RobotControl::forward(float speed)
{
	if (access)
		robotInterface->forward(speed);
	else
		cout << "You don't have access. Open system preferences for access" << endl;
}

void RobotControl::backward(float speed)
{
	if (access)
		robotInterface->backward(speed);
	else
		cout << "You don't have access. Open system preferences for access" << endl;
}

void RobotControl::stopMove()
{
	if (access)
		robotInterface->stopMove();
	else
		cout << "You don't have access. Open system preferences for access" << endl;

}

void RobotControl::setState(int value)
{
	if (access)
		robotInterface->setState(value);
	else
		cout << "You don't have access. Open system preferences for access" << endl;
}

bool RobotControl::addToPath()
{
	if (access)
	{
		path->addPos(getPose());
		return true;
	}
	else
	{
		cout << "You don't have access. Open system preferences for access" << endl;
		return false;
	}
}

bool RobotControl::clearPath()
{
	if (access)
	{
		for (int i = 0; i < path->getLength(); i++)
		{
			path->removePos(i);
		}
		return true;
	}
	else
	{
		cout << "You don't have access. Open system preferences for access" << endl;
		return false;
	}
}

bool RobotControl::recordPath()
{
	if (access)
	{
		Pose p;
		record->openFile();
		for (int i = 0; i < path->getLength(); i++)
		{
			p = path->getPos(i);
			*record << p;
		}
		record->closeFile();
		return true;
	}
	else
	{
		cout << "You don't have access. Open system preferences for access" << endl;
		return false;
	}
}

void RobotControl::setFileName(string filename)
{
	record->setFileName(filename);
}

void RobotControl::stopTurn()
{
	if (access)
		robotInterface->stopTurn();
	else
		cout << "You don't have access. Open system preferences for access" << endl;
}

Pose RobotControl::getPose()
{
	if (access)
	{
		return robotInterface->getPose();
	}
	else
	{
		cout << "You don't have access. Open system preferences for access" << endl;
		return NULL;
	}
}

void RobotControl::setPose(Pose* pos)
{
	if (access)
	{
		robotInterface->setPose(pos);
	}
	else
	{
		cout << "You don't have access. Open system preferences for access" << endl;
	}
}

RobotControl::RobotControl(PioneerRobotAPI* robot)
{
	robotOperator = new RobotOperator();
	record = new Record;
	robotInterface = new PioneerRobotInterface(robot);
	path = new Path;
}

void RobotControl::print()
{
	if (access)
	{
		robotInterface->print();
	}
	else
	{
		cout << "You don't have access. Open system preferences for access" << endl;
	}
}

bool RobotControl::openAccess(int code)
{
	access = robotOperator->checkAccessCode(code);
	return access;
}

bool RobotControl::closeAccess(int code)
{
	if (access == true && robotOperator->checkAccessCode(code)) {
		access = false;
		return true;
	}
	else if (!access && robotOperator->checkAccessCode(code)) {
		cout << "Access is already close. You have to open it first" << endl;
		return false;
	}
	else if (!robotOperator->checkAccessCode(code)) {
		cout << "You have entered wrong password!" << endl;
		return false;
	}
	else
		return false;
}


