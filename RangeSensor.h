/**
*	@file   RangeSensor.h
*	@Author �a�la �elik (152120131104)
*	@date   January 24,2021
*   @brief  Header of RangeSensor class.
 */
#pragma once
#include "PioneerRobotAPI.h"

 /**This class includes pure virtual methods.
*/

class RangeSensor
{
protected:
	float ranges[181];
	PioneerRobotAPI* robotAPI;
public:
	virtual float getRange(int index)=0;
	virtual float getMax(int& index)=0;
	virtual float getMin(int& index)=0;
	virtual void updateSensor(float ranges[])=0;
	virtual float operator[](int i)=0;
	virtual float getAngle(int index)=0;
};

