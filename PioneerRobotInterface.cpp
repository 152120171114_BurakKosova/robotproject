#include "PioneerRobotInterface.h"

PioneerRobotInterface::PioneerRobotInterface(PioneerRobotAPI* robot)
{
	position = new Pose;
	robotAPI = robot;
	state = 0;
}

PioneerRobotInterface::~PioneerRobotInterface()
{
	delete robotAPI;
}

void PioneerRobotInterface::print()
{
	std::cout << "Positions :" << robotAPI->getX() << "  " << robotAPI->getY() << "  " << robotAPI->getTh() << std::endl;
}

void PioneerRobotInterface::turnLeft()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
}

void PioneerRobotInterface::turnRight()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
}

void PioneerRobotInterface::forward(float speed)
{
	robotAPI->moveRobot(speed);
}

void PioneerRobotInterface::backward(float speed)
{
	robotAPI->moveRobot(-speed);
}

Pose PioneerRobotInterface::getPose()
{
	position->setPose(robotAPI->getX(),robotAPI->getY(),robotAPI->getTh());
	return *position;
}

void PioneerRobotInterface::setPose(Pose* pos)
{
	robotAPI->setPose(pos->getX(), pos->getY(), pos->getTh());
}

void PioneerRobotInterface::stopTurn()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::forward);
}

void PioneerRobotInterface::stopMove()
{
	robotAPI->moveRobot(0);
}

void PioneerRobotInterface::setState(int value)
{
	state = value;
}

void PioneerRobotInterface::updateSensor(float ranges[])
{
	rangeSensor->updateSensor(ranges);
}
