/**
* @file RobotControl.cpp
* @author �ulenur ��rez (152120181038)
* @date 23.01.2021
* @brief Source file of RobotControl class.
*
*	This file includes all the declarations of the RobotControl class.
*/

#pragma once
//!  RobotControl is a class that controls all kinds of robot movements.
/*!
* Takes the robot and allows us to do operations on it.
* If acces is opened, you can use all functions in this class, otherwise no function will be work.
* */
#include "PioneerRobotInterface.h"
#include "RangeSensor.h"
#include "Path.h"
#include "Record.h"
#include "RobotOperator.h"
#include <iostream>;
class RobotControl
{
private:
	PioneerRobotInterface* robotInterface;
	RangeSensor* rangeSensor;
	Path* path;
	Record* record;
	RobotOperator* robotOperator;
	bool access;

public:
	/** Constructor for RobotControl class
	* @param robot is the robot.
	*/
	RobotControl(PioneerRobotAPI* robot);

	/** Prints the positions of the robot.
	*/
	void print();

	/** Function compares the input code and the password that is saved in encryption class
	* @param code is the code that is user input.
	* @return access state.
	*/
	bool openAccess(int code);

	/** Function compares the input code and the password that is saved in encryption class
	* @param code is the code that is user input.
	* @return access state.
	*/
	bool closeAccess(int code);

	/** This function turns the robot left from the functions in PionerRobotAPI.h file.
	* Uses turnRobot function and gives that function the direction left.
	*/
	void turnLeft();

	/** This function turns the robot right from the functions in PionerRobotAPI.h file.
	* Uses turnRobot function and gives that function the direction right.
	*/
	void turnRight();

	/** Allows the robot to move forward at the desired speed.
	* Uses moveRobot function and this function takes the speed.
	* @param speed is the speed of the robot that we want.
	*/
	void forward(float speed);

	/** Allows the robot to move backward at the desired speed.
	* Uses moveRobot function and this function takes the speed.
	* Speed will be given negative sign to the function.
	* @param speed is the speed of the robot that we want.
	*/
	void backward(float speed);

	/** Gets the pose class.
	* @return Pose object from the RobotInterface class.
	*/
	Pose getPose();

	/** Sets the pose class.
	* @param pos is the position that is desired of the robot.
	*/
	void setPose(Pose* pos);

	/** Allows the robot to stop its turn.
	* If robot is moving and turning, this function only stops turning of the robot.
	* Uses moveRobot function and gives it "0" for the parameter
	*/
	void stopTurn();

	/** Allows the robot to stop its move.
	* If robot is moving and turning, this function only stops moving backward or forward of the robot.
	* Uses turnRobot function and gives it forward direction for the parameter.
	*/
	void stopMove();

	/** Setting the state of the robot.
	* Function is called in constructor and state will be 1.
	* @param value is for the set. 
	*/
	void setState(int value);

	/** Saves the pose to the path.
	* Uses addToPath function in Path class.
	* Gives the current position to the addToPath function with the member function getPose();
	*/
	bool addToPath();

	/** Clears the path
	* Uses getLength and removePos functions of the Path class.
	* Until the path length, calls the removePos function of Path class.
	*/
	bool clearPath();

	/** Writes the path to the file
	* Uses << operator of Record class.
	* Writes the poses to the file.
	*/
	bool recordPath();

	/** Sets the filename
	* @param filename is the filename that is desired to write.
	*/
	void setFileName(string filename="file.txt");
};