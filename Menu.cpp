/**
* @file Menu.cpp
* @date 23.01.2021
* @brief Menu of the robot.
*
*	Menu of the robot that contains all functions and controls of the robot.
*   (Password of the access : 1234).
*/

//!  Robot menu.
/*!
* Robot can be controlled by user in this menu easily. There is a good design.
* */
#include "PioneerRobotAPI.h"
#include "RobotControl.h"
#include <iostream>
#include "SonarSensor.h"
#include "LaserSensor.h"
using namespace std;

PioneerRobotAPI* robot;

/**Prints main menu
*/
void printMainMenu();

/**Prints connection menu
*/
void printConnectionMenu();

/**Prints motion menu
*/
void printMotionMenu();

/**Prints sensor menu
*/
void printSensorMenu();

/**Prints sonar sensor menu
*/
void printSonarSensor();

/**Prints laser sensor menu
*/
void printLaserSensor();

/**Prints path%record menu
*/
void printPathMenu();

int main()
{
	robot = new PioneerRobotAPI;
	RobotControl robotControl(robot);
	SonarSensor sonarsensor(robot);
	LaserSensor lasersensor(robot);
	cout << "(Access password!! : 1234)" << endl;
	cout << "(Access password!! : 1234)" << endl;
	cout << "(Access password!! : 1234)" << endl;
	int choose;
	int choose2;
	int choose3;
	for (int i = 0;;)
	{
		if (i == 0)
		{
			printMainMenu();
			cin >> choose;
			if (choose == 1)
			{
				i++;
			}
			if (choose == 2)
			{
				i++;
			}
			if (choose == 3)
			{
				i++;
			}
			if (choose == 4)
			{
				int code;
				while (true)
				{
					cout << "Enter password: ";
					cin >> code;
					if (robotControl.openAccess(code))
					{
						cout << "Access granted." << endl;
						break;
					}
					else
					{
						int choose4;
						cout << "You don't have access to control the robot. If you want to try again press 1 or you can continue (0)" << endl;
						cin >> choose4;

						if (choose4 == 0) {
							break;
						}
					}
				}
				
			}

			if (choose == 5)
			{
				int code;
				while (true)
				{
					cout << "Enter password to close access: ";
					cin >> code;
					if (robotControl.closeAccess(code))
					{
						cout << "See you later." << endl;
						break;
					}
					else
					{
						int choose4;
						cout << "If you want to try again press 1 or you can continue (0)" << endl;
						cin >> choose4;

						if (choose4 == 0) {
							break;
						}
					}
				}
			}

			if (choose == 6)
			{
				i++;
			}
			
			if (choose == 7)
			{
				system("pause");
				return 0;
			}
		}
		if (i == 1 && choose == 1)
		{
			printConnectionMenu();
			cin >> choose2;
			if (choose2 == 1)
			{
				if (!robot->connect()) {
					cout << "Could not connect..." << endl;
					return 0;
				}
				cout << "<connect>" << endl;
				cout << "Robot connected. " << endl << endl;
				robotControl.setState(1);
				//connect robot
			}
			if (choose2 == 2)
			{
				robot->disconnect();
				cout << "<disconnect>" << endl;
				cout << "Robot disconnected. " << endl << endl;
				robotControl.setState(0);
				//disconnect
			}
			if (choose2 == 3)
			{
				//back
				i -= 1;
			}
		}
		if (i == 1 && choose == 2)
		{
			printMotionMenu();
			cin >> choose2;
			if (choose2 == 1)
			{
				cout << "Enter speed (mm/s)" << endl;
				float speed;
				cin >> speed;
				robotControl.forward(speed);
			}
			if (choose2 == 2)
			{
				robotControl.stopMove();
			}
			if (choose2 == 3)
			{
				robotControl.turnLeft();
			}
			if (choose2 == 4)
			{
				robotControl.turnRight();
			}
			if (choose2 == 5)
			{
				robotControl.stopTurn();
			}
			if (choose2 == 6)
			{
				//back
				i -= 1;
			}
		}

		if (i == 1 && choose == 3)
		{
			// Sensor
			printSensorMenu();
			cin >> choose2;
			if (choose2 == 1)
			{
				printSonarSensor();
				cin >> choose3;
				if (choose3 == 1)
				{
					cout << "Enter index between 0 and 15" << endl;
					int c;
					cin >> c;
					cout << "Sonar index " << c;
					cout << " range : " << sonarsensor.getRange(c) << endl;
				}
				if (choose3 == 2)
				{
					int index = 0;
					float value=sonarsensor.getMin(index);
					cout << "Minimum sonar range index: " << index << "   Min sonar range : " << value << endl;
					//
				}
				if (choose3 == 3)
				{
					int index = 0;
					float value = sonarsensor.getMax(index);
					cout << "Maximum sonar range index: " << index << "   Max sonar range : " << value << endl;
				}
				if (choose3 == 4)
				{
					i -= 1;
				}

			}
			if (choose2 == 2)
			{
				printLaserSensor();
				cin >> choose3;
				if (choose3 == 1)
				{
					cout << "Enter index between 0 and 180" << endl;
					int c;
					cin >> c;
					cout << "Laser index " << c;
					cout << " range : " << lasersensor.getRange(c) << endl;
					//
				}
				if (choose3 == 2)
				{
					int index = 0;
					float value = lasersensor.getMin(index);
					cout << "Minimum laser range index: " << index << "   Min laser range : " << value << endl;
					//
				}
				if (choose3 == 3)
				{
					int index = 0;
					float value = lasersensor.getMin(index);
					cout << "Maximum laser range index: " << index << "   Max laser range : " << value << endl;
					//
				}
				if (choose3 == 4)
				{
					i -= 1;
				}

			}

			if (choose2 == 3)
			{
				i -= 1;
			}

		}
		if (i == 1 && choose == 6)
		{
			printPathMenu();
			int choose6;
			cin >> choose6;
			if (choose6 == 1)
			{
				if (!robotControl.addToPath())
				{
					cout << "There is an unknown error." << endl;
				}
				else cout << "Current position is saved." << endl;
				
			}
			if (choose6 == 2)
			{
				robotControl.clearPath();
				cout << "Path is cleared." << endl;
			}
			if (choose6 == 3)
			{
				string filename;
				cout << "Enter the filename that you want to write: ";
				cin >> filename;
				robotControl.setFileName(filename);
				if (!robotControl.recordPath())
				{
					cout << "Error while opening/writing" << endl;
				}
				else cout << "Path is writed to the file that named <" << filename << ">" << endl;

			}
			if (choose6 == 4)
			{
				i -= 1;
			}
		}
	}


}

void printMainMenu()
{
	cout << "Main Menu" << endl;
	cout << "1. Connection" << endl;
	cout << "2. Motion" << endl;
	cout << "3. Sensor" << endl;
	cout << "4. Open Access" << endl;
	cout << "5. Close Access" << endl;
	cout << "6. Path&Record" << endl;
	cout << "7. Quit" << endl;
	cout << "Choose one : " << endl;
}

void printConnectionMenu()
{
	cout << "Connection Menu" << endl;
	cout << "1. Connect Robot" << endl;
	cout << "2. Disconnect Robot" << endl;
	cout << "3. Back" << endl;
	cout << "Choose one : " << endl;
}

void printMotionMenu()
{
	cout << "Motion Menu" << endl;
	cout << "1. Move robot" << endl;
	cout << "2. Stop move robot(only forward or backward)" << endl;
	cout << "3. Turn left" << endl;
	cout << "4. Turn right" << endl;
	cout << "5. Stop turn robot" << endl;
	cout << "6. Back" << endl;
	cout << "Choose one : " << endl;
}

void printPathMenu()
{
	cout << "Path&Record Menu" << endl;
	cout << "1. Save current position of the robot to the path" << endl;
	cout << "2. Delete positions that are saved to path" << endl;
	cout << "3. Write the position(s)/path to the file" << endl;
	cout << "4. Back" << endl;
	cout << "Choose one : " << endl;
}

void printSensorMenu()
{
	cout << "Sensor Menu" << endl;
	cout << "1. Sonar sensor" << endl;
	cout << "2. Laser sensor" << endl;
	cout << "3. Back" << endl;
	cout << "Choose one : " << endl;

}

void printSonarSensor()
{
	cout << "Sonar Sensor Menu" << endl;
	cout << "1. Range" << endl;
	cout << "2. Minimum" << endl;
	cout << "3. Maximum" << endl;
	cout << "4. Back" << endl;
}

void printLaserSensor()
{
	cout << "Laser Sensor Menu" << endl;
	cout << "1. Range" << endl;
	cout << "2. Minimum" << endl;
	cout << "3. Maximum" << endl;
	cout << "3. Back" << endl;
}