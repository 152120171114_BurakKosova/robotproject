/**
* @file PioneerRobotInterface.h
* @author �ulenur ��rez (152120181038),Burak Kosova(152120171114),Hande Birsen(152120181107),�a�la �elik(152120131104)
* @date 24.01.2021
* @brief Header of PioneerRobotInterface class.
*
*	This file includes all the declarations of member variables and functions for PioneerRobotInterface class.
*/

#pragma once
#include "PioneerRobotAPI.h"
#include "RobotInterface.h"

class PioneerRobotInterface : RobotInterface
{
	PioneerRobotAPI* robotAPI; //!< The robot that takes in the constructor will be equal that robotAPI and the processes will continue with that robotAPI.

public:
	/** Every class that contains robotAPI, must take same robot for the constructor.
	* Creating a new robot when different classes are called is not what is desired.
	* @param robot is required for constructor to control same robot in the main from every class.
	*/
	PioneerRobotInterface(PioneerRobotAPI* robot);

	/**
	* Deletes the robotAPI in the destructor.
	*/
	~PioneerRobotInterface();

	/** Prints the positions of the robot.
	*/
	void print();

	/** This function turns the robot left from the functions in PionerRobotAPI.h file.
	* Uses turnRobot function and gives that function the direction left.
	*/
	void turnLeft();

	/** This function turns the robot right from the functions in PionerRobotAPI.h file.
	* Uses turnRobot function and gives that function the direction right.
	*/
	void turnRight();

	/** Allows the robot to move forward at the desired speed.
	* Uses moveRobot function and this function takes the speed.
	* @param speed is the speed of the robot that we want.
	*/
	void forward(float speed);

	/** Allows the robot to move backward at the desired speed.
	* Uses moveRobot function and this function takes the speed.
	* Speed will be given negative sign to the function.
	* @param speed is the speed of the robot that we want.
	*/
	void backward(float speed);

	/** Gets the pose class.
	* @return Pose object in the RobotControl.
	*/
	Pose getPose();

	/** Sets the pose class.
	* @param pos is the position that is desired of the robot.
	*/
	void setPose(Pose* pos);

	/** Allows the robot to stop its turn.
	* If robot is moving and turning, this function only stops turning of the robot.
	* Uses moveRobot function and gives it "0" for the parameter
	*/
	void stopTurn();

	/** Allows the robot to stop its move.
	* If robot is moving and turning, this function only stops moving backward or forward of the robot.
	* Uses turnRobot function and gives it forward direction for the parameter.
	*/
	void stopMove();

	void setState(int value);

	void updateSensor(float ranges[]);
};

