/**
* @file RobotInterface.h
* @author �ulenur ��rez (152120181038),Burak Kosova(152120171114),Hande Birsen(152120181107),�a�la �elik(152120131104)
* @date 24.01.2021
* @brief Header of Record class.
*
*	This file includes all the declarations of member variables and functions for Record class.
*/

#pragma once
#include "Pose.h"
#include "RangeSensor.h"

/**This class includes pure virtual methods.
*/

class RobotInterface
{
protected:
	int state;			//!< This number represents the state of the robot.
	Pose* position;		//!< This class includes position and RobotControl class can make differences in that class.
	RangeSensor* rangeSensor;

public:
	virtual void print() = 0;

	virtual void turnLeft() = 0;

	virtual void turnRight() = 0;

	virtual void forward(float speed) = 0;

	virtual void backward(float speed) = 0;

	virtual Pose getPose() = 0;

	virtual void setPose(Pose* pos) = 0;

	virtual void stopTurn() = 0;

	virtual void stopMove() = 0;

	virtual void setState(int value) = 0;

	virtual void updateSensor(float ranges[]) = 0;
};

